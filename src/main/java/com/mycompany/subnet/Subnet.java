/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.subnet;

/**
 *
 * @author nipit
 */
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Subnet {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter IP address: ");
        String ipAddress = scanner.nextLine();
        System.out.print("Enter subnet mask in CIDR notation (e.g., /24): ");
        String subnetMask = scanner.nextLine();

        try {
            InetAddress networkAddress = calculateNetworkAddress(ipAddress, subnetMask);
            InetAddress broadcastAddress = calculateBroadcastAddress(ipAddress, subnetMask);
            InetAddress firstUsableIP = calculateFirstUsableIP(ipAddress, subnetMask);
            InetAddress lastUsableIP = calculateLastUsableIP(ipAddress, subnetMask);

            System.out.println("Network Address: " + networkAddress.getHostAddress());
            System.out.println("Broadcast Address: " + broadcastAddress.getHostAddress());
            System.out.println("First Usable IP: " + firstUsableIP.getHostAddress());
            System.out.println("Last Usable IP: " + lastUsableIP.getHostAddress());
        } catch (UnknownHostException e) {
            System.err.println("Invalid input: " + e.getMessage());
        }
    }

    private static InetAddress calculateNetworkAddress(String ipAddress, String subnetMask) throws UnknownHostException {
        InetAddress ip = InetAddress.getByName(ipAddress);
        int prefixLength = Integer.parseInt(subnetMask.substring(1));
        int mask = 0xffffffff << (32 - prefixLength);
        byte[] networkBytes = ip.getAddress();

        for (int i = 0; i < 4; i++) {
            networkBytes[i] &= (mask >> (24 - 8 * i)) & 0xff;
        }

        return InetAddress.getByAddress(networkBytes);
    }

    private static InetAddress calculateBroadcastAddress(String ipAddress, String subnetMask) throws UnknownHostException {
        InetAddress networkAddress = calculateNetworkAddress(ipAddress, subnetMask);
        int prefixLength = Integer.parseInt(subnetMask.substring(1));
        int hostBits = 32 - prefixLength;
        int mask = (1 << hostBits) - 1;
        byte[] broadcastBytes = networkAddress.getAddress();

        for (int i = 0; i < 4; i++) {
            broadcastBytes[i] |= mask >> (24 - 8 * i);
        }

        return InetAddress.getByAddress(broadcastBytes);
    }

    private static InetAddress calculateFirstUsableIP(String ipAddress, String subnetMask) throws UnknownHostException {
        InetAddress networkAddress = calculateNetworkAddress(ipAddress, subnetMask);
        byte[] firstUsableBytes = networkAddress.getAddress();
        firstUsableBytes[3]++;

        return InetAddress.getByAddress(firstUsableBytes);
    }

    private static InetAddress calculateLastUsableIP(String ipAddress, String subnetMask) throws UnknownHostException {
        InetAddress broadcastAddress = calculateBroadcastAddress(ipAddress, subnetMask);
        byte[] lastUsableBytes = broadcastAddress.getAddress();
        lastUsableBytes[3]--;

        return InetAddress.getByAddress(lastUsableBytes);
    }
}
